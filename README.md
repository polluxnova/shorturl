# Test project. Implementation of the url-shortener.

## Components:
### 1. api (.NET Core web api)
### 2. db (Postgres database)


To run:
```
1) mkdir -p db/data (ensure there is an empty data directory inside db directory, first time only)
2) docker-compose up

```


To use:
```
1) curl -XPOST http://localhost:9000/urls/?longUrl=http%3A%2F%2Fsearch.yahoo.com%2Fsearch%3Fp%3Danything -H 'Content-type:application/json' -d '' -i
2) curl -XGET http://localhost:9000/urls/1 -i

```

##Implementation summary:
API exposes two methods with /urls controller: 
1) to GET long url by short key. The short key is implemented as a Base62 encoded integer/long value which is a primary key of the record in DB.
The GET method can handle optional "forward" parameter which would instruct it to return HTTP 302 status code instead of 200. 
2) to add long url into the storage (returning short key)


##Comments/TODO:
1) Ideally building of API sub-project has to be done on the build server. Currently the intermediate builder container is used (see api/Dockerfile) which is consuming more time and HDD space.
2) DB can be clusterized to improve scalability and availability (TODO feature)
3) Unit/integration tests can be added (TODO). 
4) API could be documented with Swagger (TODO). 
5) Handle other Content-type(s) for POST method (TODO).
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace shorturl.api.Utils
{
    public class BaseX
    {
        static char[] Vocabulary = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                                                'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};

        static Dictionary<char, int> VocabularyIndex = BuildVocabularyIndex();

        static Dictionary<char, int> BuildVocabularyIndex()
        {
            Dictionary<char, int> res = new Dictionary<char, int>();
            int i = 0;
            foreach(char c in Vocabulary)
            {
                res.Add(c, i++);
            }
            return res;
        }

        public static string Encode(ulong x)
        {
            StringBuilder sb = new StringBuilder();
            ulong bs = (ulong)Vocabulary.Length;
            while(x > bs)
            {
                ulong x1 = x / bs;
                sb.Insert(0, Vocabulary[x - x1*bs]);
                x = x1;
            }
            sb.Insert(0, Vocabulary[x]);
            return sb.ToString();
        }

        public static ulong Decode(string s)
        {
            ulong res = 0;
            try
            {                
                char c;
                int v;
                for (int i = 0; i < s.Length; i++)
                {
                    c = s[s.Length - i - 1];
                    v = VocabularyIndex[c];
                    res += (ulong)(Math.Pow(Vocabulary.Length, i) * v);
                }
            }
            catch
            {
            }
            return res;
        }
    }
}

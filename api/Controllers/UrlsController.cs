﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Mvc;
using shorturl.api.Services;
using shorturl.api.Utils;

namespace shorturl.api.Controllers
{
    [Route("/[controller]")]
    public class UrlsController : Controller
    {
        #region DI
        IDataService _storage;

        public UrlsController(IDataService storage)
        {
            _storage = storage;
        }
        #endregion

        // POST /urls
        [HttpPost]
        public ActionResult Post(string longUrl)
        {
            if(!IsValidUrl(longUrl))
                return BadRequest();

            ulong id = _storage.SaveUrl(longUrl);             

            return Ok(BaseX.Encode(id));
        }

        // GET /urls/xYz52ja
        [HttpGet("{id}")]
        public ActionResult Get(string id, int? forward)
        {
            if (!IsValidKey(id))
                return BadRequest();

            ulong key = BaseX.Decode(id);
            if (key == 0)
                return BadRequest();

            string url = _storage.GetUrl(key);

            if(string.IsNullOrEmpty(url))
                return NotFound();

            if (forward.HasValue)
                return Redirect(url);

            return Ok(url);                                       
        }

        #region validation
        private bool IsValidUrl(string url)
        {
            string urlPattern = @"^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?";
            return !string.IsNullOrEmpty(url) && Regex.IsMatch(url, urlPattern);
        }

        private bool IsValidKey(string key)
        {
            return !string.IsNullOrEmpty(key) && key.Length <= 11;
        }
        #endregion
    }
}

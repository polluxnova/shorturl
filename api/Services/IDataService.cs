﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace shorturl.api.Services
{
    public interface IDataService
    {
        ulong SaveUrl(string url);
        string GetUrl(ulong key);
    }
}

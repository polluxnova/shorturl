﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace shorturl.api.Services
{
    public class DataService : IDataService
    {
        IConfiguration _config;

        public DataService(IConfiguration config)
        {
            _config = config;
        }

        public string GetUrl(ulong key)
        {
            string res = null;
            ExecCommand((cmd) =>
            {
                cmd.CommandText = "get_url";
                cmd.Parameters.Add(new Npgsql.NpgsqlParameter("key", (long)key));
                var r = cmd.ExecuteScalar();
                res = r is DBNull ? null : (string)r;
            });            
            return res;
        }


        public ulong SaveUrl(string url)
        {
            ulong res = 0;
            ExecCommand((cmd) =>
            {
                cmd.CommandText = "add_url";
                cmd.Parameters.Add(new Npgsql.NpgsqlParameter("url", url));
                var r = cmd.ExecuteScalar();
                res = (ulong)(int)r;
            });
            return res;
        }



        public delegate void CommandDelegate(Npgsql.NpgsqlCommand cmd);

        public void ExecCommand(CommandDelegate internals)
        {
            try
            {
                string connString = _config.GetConnectionString("live_db");
                using (var conn = new Npgsql.NpgsqlConnection(connString))
                {
                    conn.Open();
                    Npgsql.NpgsqlCommand cmd = new Npgsql.NpgsqlCommand("", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    internals(cmd);                    
                }
            }
            catch (Exception ex)
            {
                //log
                throw new Exception("Storage failure");
            }
        }
    }
}

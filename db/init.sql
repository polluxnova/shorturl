SELECT '--Begin init.sql script--';

CREATE USER srvc_usr WITH PASSWORD 'astroMagic092';

CREATE TABLE urls(id serial PRIMARY KEY, url text not null);

GRANT SELECT, INSERT ON urls TO srvc_usr;
GRANT USAGE, SELECT ON SEQUENCE urls_id_seq TO srvc_usr;


CREATE FUNCTION add_url(in url text) RETURNS integer
    AS $$ INSERT INTO urls(url) VALUES (url) RETURNING id $$
    LANGUAGE SQL;

CREATE FUNCTION get_url(in key bigint) RETURNS text
    AS $$ SELECT url FROM urls WHERE id=key $$
    LANGUAGE SQL;
